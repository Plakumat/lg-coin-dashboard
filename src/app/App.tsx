import React from 'react';

// components
import Dashboard from '../pages/Dashboard';

const App: React.FC = () => {
  return (
    <div className='app'>
      <Dashboard />
    </div>
  );
};

export default App;
