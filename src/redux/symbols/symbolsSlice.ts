// redux
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

// models
import { SymbolsState } from './model/symbolsState';

// axios
import axios from 'axios';

// utils
import { CONSTANTS } from '../../app/constants';

const initialState: SymbolsState = {
  symbols: {
    getAllSymbols: [],
  },
};

export const getSymbols: any = createAsyncThunk('get/getSymbols', async (options: { getData: false }, { getState, dispatch }) => {
  const state = getState() as SymbolsState;
  let allSymbols: any = state.symbols.getAllSymbols;

  await axios
    .get(CONSTANTS.API_URL)
    .then(function (response) {
      allSymbols = response.data;
    })
    .catch(function (error) {
      console.log(error);
    });

  return allSymbols;
});

export const symbolsSlice = createSlice({
  name: 'symbols',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getSymbols.fulfilled, (state, { payload }: any) => {
      state.symbols.getAllSymbols = payload;
    });
  },
});

export default symbolsSlice.reducer;
