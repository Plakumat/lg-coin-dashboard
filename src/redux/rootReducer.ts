import { AnyAction, combineReducers, CombinedState } from 'redux';
import persistReducer, { PersistPartial } from 'redux-persist/es/persistReducer';
import storage from 'redux-persist/es/storage';

// reducers
import symbolReducer from './symbols/symbolsSlice';
import { SymbolsState } from './symbols/model/symbolsState';

const symbolPersistConfig = {
  key: 'symbols',
  storage,
  keyPrefix: 'redux-',
  blacklist: ['symbols'],
};

const combinedReducer = combineReducers({
  symbols: persistReducer(symbolPersistConfig, symbolReducer),
});

const rootReducer = (
  state:
    | CombinedState<{
        symbols: SymbolsState & PersistPartial;
      }>
    | undefined,
  action: AnyAction,
) => {
  return combinedReducer(state, action);
};

export { symbolPersistConfig, rootReducer, persistReducer };
