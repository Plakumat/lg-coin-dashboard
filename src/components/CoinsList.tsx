import React from 'react';

// antd
import { Modal, Input } from 'antd';

// utils
import { CONSTANTS } from '../app/constants';

// redux
import { useAppSelector } from '../redux/store';

interface ICoinsListProps {
  modalIsOpen: boolean;
  cancelHandle: () => void;
  searchHandle: (event: any) => void;
  pageSizeHandle: (event: any) => void;
  listTemplateHandle: (index: number, coin: any) => any;
  coinsSearched: any;
  activeSearchTerms: string;
  listLimit: number;
}

const CoinsList = (props: ICoinsListProps) => {
  const { modalIsOpen, cancelHandle, searchHandle, pageSizeHandle, listTemplateHandle, coinsSearched, activeSearchTerms, listLimit } = props;
  const GetAllSymbols = useAppSelector((state) => state.symbols.symbols.getAllSymbols);

  return (
    <Modal destroyOnClose={true} width={1000} title={CONSTANTS.MODAL_TITLE} open={modalIsOpen} onOk={cancelHandle} onCancel={cancelHandle}>
      <Input placeholder={CONSTANTS.SEARCH_PLACEHOLDER.toString()} onInput={(event: React.KeyboardEvent<HTMLInputElement>) => searchHandle(event)} />
      <div className='symbols' onScroll={pageSizeHandle} data-symbols-list>
        {GetAllSymbols && !(activeSearchTerms.length > CONSTANTS.MIN_SEARCH_ITEM) ? (
          GetAllSymbols.slice(0, listLimit).map((symbol: any, index: number) => {
            return listTemplateHandle(index, symbol);
          })
        ) : activeSearchTerms.length > CONSTANTS.MIN_SEARCH_ITEM && coinsSearched.length > 0 ? (
          coinsSearched.slice(0, listLimit).map((symbol: any, index: number) => {
            return listTemplateHandle(index, symbol);
          })
        ) : !GetAllSymbols ? (
          <p className='symbols__not-found'>{CONSTANTS.NO_FOUND_TO_SHOW}</p>
        ) : (
          <p className='symbols__not-found'>{`'${activeSearchTerms}' ${CONSTANTS.NO_FOUND_FOR_TO_SHOW}`}</p>
        )}
      </div>
    </Modal>
  );
};

export default CoinsList;
