import React from 'react';

// antd
import { Button } from 'antd';

// utils
import { CONSTANTS } from '../app/constants';

// components
import PieChart from './Chart';

interface IMyCoins {
  modalVisibleHandle: () => void;
  refreshButtonHandle: () => void;
  listTemplateHandle: (index: number, coin: any) => any;
  slices: Array<Array<string | number>>;
  currentCoins: any;
}

const MyCoins = (props: IMyCoins) => {
  const { modalVisibleHandle, refreshButtonHandle, listTemplateHandle, slices, currentCoins } = props;

  return (
    <section className='coins-area'>
      <div className='coins-area__buttons'>
        <Button className='coins-area__buttons__item' onClick={modalVisibleHandle}>
          {CONSTANTS.ADD_STOCK}
        </Button>
        <Button className='coins-area__buttons__item' onClick={refreshButtonHandle}>
          {CONSTANTS.REFRESH}
        </Button>
      </div>
      <div className='coins-area__wrapper'>
        <div className='coins-area__list' data-coin-area-list>
          <>{currentCoins && currentCoins.map((coin: any, index: number) => listTemplateHandle(index, coin))}</>
        </div>
        <div className='coins-area__chart'>
          <p className='coins-area__chart__title'>{CONSTANTS.MY_COINS}</p>
          {slices && slices.length > 1 ? (
            <PieChart width={'100%'} height={'100%'} chartData={slices} />
          ) : (
            <div className='coins-area__chart__no-data'>
              <svg height='400' width='400'>
                <circle cx='200' cy='200' r='200' fill='lightgray' />
              </svg>
            </div>
          )}
        </div>
      </div>
    </section>
  );
};

export default MyCoins;
