import React from 'react';
import { Chart } from 'react-google-charts';

interface IPieChartProps {
  title?: string;
  chartData: Array<Array<string | number>>;
  width?: number | string;
  height?: number | string;
}

const PieChart = (props: IPieChartProps) => {
  const { title, chartData, ...rest } = props;

  const options = {
    title: title,
    is3D: true,
  };

  return <Chart chartType='PieChart' data={chartData} options={options} {...rest} />;
};

export default PieChart;
