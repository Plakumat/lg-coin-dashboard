/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';

// redux
import { getSymbols } from '../redux/symbols/symbolsSlice';
import { useAppSelector, dispatch } from '../redux/store';

// antd
import { Modal, Button } from 'antd';

// components
import MyCoins from '../components/MyCoins';
import CoinsList from '../components/CoinsList';

// utils
import { CONSTANTS } from '../app/constants';

interface ISelectedCoinsProps {
  symbol: string;
  symbolQuantity: string;
  avgPrice: string;
  lastPrc: string;
}

const Dashboard = () => {
  const GetAllSymbols = useAppSelector((state) => state.symbols.symbols.getAllSymbols);

  const [chartSlices, setChartSlices] = useState<Array<Array<string | number>>>(
    JSON.parse(localStorage.getItem(CONSTANTS.LAST_POSITIONS_STORAGE)!)?.length > CONSTANTS.CHART_BLANK_ITEM_COUNT
      ? JSON.parse(localStorage.getItem(CONSTANTS.LAST_POSITIONS_STORAGE)!)
      : [],
  );
  const [selectedCoins, setSelectedCoins] = useState<Array<ISelectedCoinsProps>>(
    JSON.parse(localStorage.getItem(CONSTANTS.SELECTED_COINS_STORAGE)!)?.length > 0
      ? JSON.parse(localStorage.getItem(CONSTANTS.SELECTED_COINS_STORAGE)!)
      : [],
  );
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [myCoins, setMyCoins] = useState<any>(
    JSON.parse(localStorage.getItem(CONSTANTS.MY_COINS_STORAGE)!)?.length > 0 ? JSON.parse(localStorage.getItem(CONSTANTS.MY_COINS_STORAGE)!) : [],
  );
  const [currentLimit, setCurrentLimit] = useState<number>(CONSTANTS.CURRENT_LIMIT);
  const [actionType, setActionType] = useState<string>('');
  const [searchTerms, setSearchTerms] = useState<string>('');
  const [searchedCoins, setSearchedCoins] = useState<any>([]);

  const findCurrentSimilarItems = () => {
    const similarItems: any = [];

    // find the coins will be listed on chart area from all symbols data
    selectedCoins.forEach((selected: ISelectedCoinsProps) => {
      const similarCoin = GetAllSymbols.find((similar: any) => similar.symbol === selected.symbol);
      similarItems.push(similarCoin);
    });

    setMyCoins(similarItems);
  };

  const symbolListItemTemplate = (index: number, data: any) => {
    if (data) {
      const defaultValue = chartSlices.find((item: any) => item[0] === data?.symbol);

      return (
        <form onSubmit={handleSubmit} className='symbols__item' key={index}>
          <p>
            <strong>{data.symbol}</strong> - {data.lastPrice}
            <br /> {data?.weightedAvgPrice}
          </p>
          <div className='symbols__item__buttons'>
            <input
              min={1}
              max={10}
              key={index}
              name='quantity'
              type='number'
              className='symbols__item__input'
              defaultValue={defaultValue ? defaultValue[1] : 1}
            />
            <input type='hidden' name='actionType' defaultValue={actionType} />
            <input type='hidden' name='symbol' defaultValue={data.symbol} />
            <input type='hidden' name='weightedAvgPrice' defaultValue={data.weightedAvgPrice} />
            <input type='hidden' name='lastPrice' defaultValue={data.lastPrice} />
            {defaultValue ? (
              <>
                <Button className='symbols__item__buttons__item' htmlType='submit' onClick={() => setActionType(CONSTANTS.ACTION_TYPE_UPDATE)}>
                  {CONSTANTS.UPDATE}
                </Button>
                <Button className='symbols__item__buttons__item' htmlType='submit' onClick={() => setActionType(CONSTANTS.ACTION_TYPE_REMOVE)}>
                  {CONSTANTS.REMOVE}
                </Button>
              </>
            ) : (
              <Button className='symbols__item__buttons__item add--button' htmlType='submit' onClick={() => setActionType(CONSTANTS.ACTION_TYPE_ADD)}>
                {CONSTANTS.ADD}
              </Button>
            )}
          </div>
        </form>
      );
    }
  };

  const handlePageSize = (event: React.UIEvent<HTMLElement>) => {
    const listArea = event.target as HTMLDivElement;

    // detect bottom of list content
    if (listArea.scrollHeight - listArea.scrollTop - listArea.clientHeight < 1) {
      setCurrentLimit(currentLimit + CONSTANTS.CURRENT_LIMIT);
    }
  };

  const handleRefreshButton = () => {
    dispatch(getSymbols());
  };

  const handleModalVisible = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);

    // reset current list limit
    setCurrentLimit(CONSTANTS.CURRENT_LIMIT);

    // remove modal from dom for reset
    Modal.destroyAll();
  };

  const handleAddToChart = (symbolName: string, quantity: string, weightedAvgPrice: string, lastPrice: string) => {
    setSelectedCoins((prevState) => [...prevState, { symbol: symbolName, symbolQuantity: quantity, avgPrice: weightedAvgPrice, lastPrc: lastPrice }]);
  };

  const handleUpdateChart = (symbolName: string, quantity: string) => {
    let newChartSlices = [];

    // slice items from current chart slice state for re-init the new values
    const willBeUpdatedSliceItem: any = chartSlices.find((selectedSlice: any) => selectedSlice[0] === symbolName);
    willBeUpdatedSliceItem[1] = parseInt(quantity);
    newChartSlices = [...chartSlices];

    // update chart slices
    const willBeUpdateSelectedItem: any = selectedCoins.find((selectedItem: any) => selectedItem.symbol === symbolName);
    willBeUpdateSelectedItem.symbolQuantity = quantity;

    // update selected coins
    setSelectedCoins((prevSelecteds) => prevSelecteds.map((selected) => (selected.symbol === symbolName ? willBeUpdateSelectedItem : selected)));

    // set new slices
    setChartSlices(newChartSlices);

    // reset coin states
    setMyCoins([]);
    setTimeout(() => findCurrentSimilarItems());
  };

  const handleRemoveFromChart = (symbolName: string) => {
    let newChartSlices = [];

    // find filtered coin from symbol name and remove it
    const filteredChart = selectedCoins.filter((e: any) => e.symbol !== symbolName);
    newChartSlices = [...filteredChart];

    setMyCoins([]);
    setSelectedCoins(newChartSlices);
  };

  const handleSubmit = (e: any) => {
    // prevent event for prevent refresh
    e.preventDefault();

    const formData: any = new FormData(e.target);
    const formProps = Object.fromEntries(formData);

    const { actionType, quantity, symbol, weightedAvgPrice, lastPrice } = formProps;

    // seperate functions by action type
    if (actionType === CONSTANTS.ACTION_TYPE_ADD) {
      handleAddToChart(symbol, quantity, weightedAvgPrice, lastPrice);
    }

    if (actionType === CONSTANTS.ACTION_TYPE_REMOVE) {
      handleRemoveFromChart(symbol);
    }

    if (actionType === CONSTANTS.ACTION_TYPE_UPDATE) {
      handleUpdateChart(symbol, quantity);
    }
  };

  const handleSearch = (event: React.KeyboardEvent<HTMLInputElement>) => {
    const searchTerm = (event.target as HTMLInputElement).value;
    setSearchTerms(searchTerm);

    // filter searched item for coin list
    const searchedItems = GetAllSymbols.filter((coin: any) => coin.symbol.toLowerCase().includes(searchTerm.toLowerCase()));
    setSearchedCoins(searchedItems);
  };

  useEffect(() => {
    // get all symbols from redux when dom ready
    dispatch(getSymbols());

    // automatic refresh
    const intervalId = setInterval(() => {
      handleRefreshButton();
    }, CONSTANTS.REFRESH_TIME);

    return () => clearInterval(intervalId);
  }, []);

  useEffect(() => {
    if (selectedCoins && selectedCoins.length > 0) {
      // set slices for chart filter by selected coins
      const preparedSlices: Array<Array<string | number>> = [];
      selectedCoins.forEach((selectedCoin: ISelectedCoinsProps, index: number) => {
        preparedSlices.push([selectedCoin.symbol, parseInt(selectedCoin.symbolQuantity)]);
      });

      // set google chart default header
      const pieHead: any = ['Task', 'Hours per Day'];
      preparedSlices.unshift(pieHead);

      // set storage for selected coins
      localStorage.setItem(CONSTANTS.SELECTED_COINS_STORAGE, JSON.stringify(selectedCoins));

      // reset coin state
      setMyCoins([]);
      setChartSlices(preparedSlices);
      findCurrentSimilarItems();

      return;
    }

    // if there is not selected coin, remove all storages
    localStorage.removeItem(CONSTANTS.SELECTED_COINS_STORAGE);
    localStorage.removeItem(CONSTANTS.LAST_POSITIONS_STORAGE);

    // reset coins state and set default header
    setMyCoins([]);
    const pieSlices = [['Task', 'Hours per Day']];
    setChartSlices(pieSlices);
  }, [selectedCoins, GetAllSymbols]);

  useEffect(() => {
    // set all coming data to storage
    if (chartSlices && chartSlices.length > CONSTANTS.CHART_BLANK_ITEM_COUNT) {
      localStorage.setItem(CONSTANTS.LAST_POSITIONS_STORAGE, JSON.stringify(chartSlices));
    }
  }, [chartSlices]);

  useEffect(() => {
    // set all coming data to storage
    if (myCoins && myCoins.length > 0) {
      localStorage.setItem(CONSTANTS.MY_COINS_STORAGE, JSON.stringify(myCoins));
      return;
    }

    localStorage.removeItem(CONSTANTS.MY_COINS_STORAGE);
  }, [myCoins]);

  return (
    <section className={`dashboard ${isModalOpen ? 'modal--active' : ''}`}>
      <MyCoins
        modalVisibleHandle={handleModalVisible}
        refreshButtonHandle={handleRefreshButton}
        listTemplateHandle={symbolListItemTemplate}
        slices={chartSlices}
        currentCoins={myCoins}
      />
      <CoinsList
        modalIsOpen={isModalOpen}
        cancelHandle={handleCancel}
        searchHandle={handleSearch}
        pageSizeHandle={handlePageSize}
        listTemplateHandle={symbolListItemTemplate}
        activeSearchTerms={searchTerms}
        listLimit={currentLimit}
        coinsSearched={searchedCoins}
      />
    </section>
  );
};

export default Dashboard;
